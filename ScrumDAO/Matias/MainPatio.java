package scrum.sprintdao.Matias;

import scrum.sprintdao.proyecto_final.patioDAO.ControlPatioDao;
import scrum.sprintdao.proyecto_final.patioDAO.ControlPatioDaoImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class MainPatio {
    public void menu(){
        System.out.println("Elige una opción:\n"
                + "1) Listar registro de patio\n"
                + "2) Selecciona un registo\n"
                + "3) Introduce un registro\n"
                + "0) Salir\n"
        );
    }
    public static void main(String[] args) throws SQLException {
        Connection c = Conexion.getConnection();
        ControlPatioDao controlPatioDao= new ControlPatioDaoImpl();
        MainPatio main =new MainPatio();
        main.menu();
        Scanner sc = new Scanner(System.in);
        int opcion = sc.nextInt(); sc.nextLine();

        while (opcion!=0){
            switch (opcion){
                case 1:{
                    System.out.println(controlPatioDao.getPatio());
                    break;
                }
                case 2:{
                    System.out.println("Introduce el id de la reclusa");
                    int idVisita = sc.nextInt();
                    System.out.println(controlPatioDao.read(idVisita));
                    break;
                }
                case 3:{
                    System.out.println("Introduce el id de la reclusa que va al patio;");
                    int idReclusa =sc.nextInt();
                    System.out.println("introduce la Hora de salida al patio");
                    String horaSalida = sc.next();
                    System.out.println("introduce la hora de regreso");
                    String horaRegreso = sc.next();
                    //System.out.println("Creado el registo"+controlPatioDao.create(new ControlPatio(0,idReclusa,horaSalida,horaRegreso)));
                    break;
                }
            }
            main.menu();
            opcion = sc.nextInt();sc.nextLine();
        }
        c.close();
    }
}
