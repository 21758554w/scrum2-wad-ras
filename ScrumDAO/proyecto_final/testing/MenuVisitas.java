package scrum.sprintdao.proyecto_final.testing;

import scrum.sprintdao.proyecto_final.conexion.Conection;
import scrum.sprintdao.proyecto_final.modelos.ControlVisitas;
import scrum.sprintdao.proyecto_final.visitasDAO.ControlVisitasDao;
import scrum.sprintdao.proyecto_final.visitasDAO.ControlVisitasDaoImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class MenuVisitas {
    public void menu(){
        System.out.println("Elige una opción:\n"
                + "1) Listar registro de visitas\n"
                + "2) Selecciona un registro\n"
                + "3) Introduce un registro\n"
                + "0) Sortir\n"
        );
    }
    public static void main(String[] args) throws SQLException {
        Connection c = Conection.getConnection();
        ControlVisitasDao controlVisitasDao= new ControlVisitasDaoImpl();
        MenuVisitas main =new MenuVisitas();
        main.menu();
        Scanner sc = new Scanner(System.in);
        int opcion = sc.nextInt(); sc.nextLine();

        while (opcion!=0){
            switch (opcion){
                case 1:{
                    System.out.println(controlVisitasDao.getRegister());
                    break;
                }
                case 2:{
                    System.out.println("Introduce el id visitas que quieres ver");
                    int idVisita = sc.nextInt();
                    System.out.println(controlVisitasDao.read(idVisita));
                    break;
                }
                case 3:{
                    System.out.println("introduce la fecha");
                    String fecha = sc.next();
                    System.out.println("introduce la hora");
                    String hora = sc.next();
                    System.out.println("Introduce el id de la reclusa a visitar;");
                    int idReclusa =sc.nextInt();
                    System.out.println("Introduce el del visitante");
                    int idVisitante= sc.nextInt();
                    System.out.println("Creado el registo"+controlVisitasDao.create(new ControlVisitas(0,fecha,hora,idReclusa,idVisitante)));
                    break;
                }
            }
            main.menu();
            opcion = sc.nextInt();sc.nextLine();
        }
        c.close();
    }
}
