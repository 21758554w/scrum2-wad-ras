package scrum.sprintdao.proyecto_final.testing;

import scrum.sprintdao.proyecto_final.reclusasDAO.ReclusasDaoImpl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class WadRasMain {
    static Scanner read = new Scanner(System.in);

    public static void main(String[] args) throws IOException, SQLException {
        WadRasMain wadRasMain = new WadRasMain();
        wadRasMain.menuWadRas();

    }

    public void menuWadRas() throws SQLException {
        MenuReclusas menuReclusas = new MenuReclusas();
        boolean salir = false;
        while (!salir) {

            int opcion = opcionesMenuWadRas();
            switch (opcion) {
                case 1:
                    System.out.println("\033[36mSELECCIONASTE LA OPCION 1:\033[0m");
                    linea();
                    menuReclusas.opciones();
                    linea();
                    break;
                case 2:
                    System.out.println("\033[36mSELECCIONASTE LA OPCION 8:\033[0m");
                    linea();

                    linea();
                    break;
                case 3:
                    System.out.println("\033[36mSELECCIONASTE LA OPCION 9:\033[0m");
                    linea();

                    linea();
                    break;
                case 4:
                    linea();

                    linea();
                    break;
                default:
                    System.out.println("");
            }
        }
    }

    public static void linea() {
        System.out.println("\033[36m--------------------------------\033[0m");
    }


    public int opcionesMenuWadRas() {
        System.out.print("\033[35mGESTION DE LA PRISION WADRAS\033[0m\n" +
                "[1]- OPCIONES RECLUSAS\n" +
                "[2]- LISTAR RECLUSAS CSV\n" +
                "[3]- INTRODUCIR NUEVAS RECLUSAS\n" +
                "[4]- LISTAR CSV + NUEVAS RECLUSAS\n" +
                "[5]- BUSCAR RECLUSA POR DNI\n" +
                "[14]- FINALIZA PROGRAMA\n" +
                "INTRODUCE UNA OPCION: ");
        return read.nextInt();
    }
}
