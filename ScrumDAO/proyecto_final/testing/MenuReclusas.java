package scrum.sprintdao.proyecto_final.testing;

import scrum.sprintdao.proyecto_final.modelos.Reclusa;
import scrum.sprintdao.proyecto_final.reclusasDAO.ReclusasDao;
import scrum.sprintdao.proyecto_final.reclusasDAO.ReclusasDaoImpl;

import java.sql.SQLException;
import java.util.Scanner;

public class MenuReclusas {
    /*
    public static void mostraTabla(DatabaseMetaData dbmd) throws SQLException {
        String[] types = {"TABLE"};
        try (ResultSet rs = dbmd.getTables(dbmd.getCatalogTerm(), dbmd.getSchemaTerm(), null, types);) {

            while (rs.next()) {
                int idReclusa = rs.getInt(1);
                String nombre = rs.getString(2);
                String apellido = rs.getString(3);
                int edad = rs.getInt(4);
                int grado = rs.getInt(5);
                int condenaRestante = rs.getInt(6);
                int visitaId = rs.getInt(7);
                System.out.println(" - Id Reclusa: " + idReclusa + "Nombre:" + nombre +
                        " Apellido: "+ apellido+" Edad: "+edad + "Grado:"+grado+"Condena Restante:"+condenaRestante+"Id Visita"+visitaId);
            }
        }
    }
*/
    public void menu(){
        System.out.print("Diguis quina opció vols executar:\n"
                + "1) Llista reclusas\n"
                + "2) Selecciona una reclusa\n"
                + "3) Introdueix reclusa\n"
                + "4) Modifica reclusa\n"
                + "5) Elimina reclusa\n"
                + "6) Sortir\n"
        );
    }
    public void opciones()throws SQLException {
        ReclusasDao reclusasDao = new ReclusasDaoImpl();
        MenuReclusas menuReclusas = new MenuReclusas();
        menuReclusas.menu();

        Scanner sc = new Scanner(System.in);
        int opcio = sc.nextInt();

        while (opcio!=6){
            switch(opcio){
                case 1-> {
                    System.out.println(reclusasDao.getReclusas());
                    break;
                }
                case 2->{
                    System.out.println("Introduce el Id de la reclusa que deseas ver");
                    int idAlbum = sc.nextInt();sc.nextLine();
                    System.out.println(reclusasDao.read(idAlbum));
                    break;
                }
                case 3->{
                    System.out.println("Introduce el nombre nuevo");
                    String nom = sc.nextLine();
                    System.out.println("Introduce el apellido nuevo");
                    String cognom = sc.nextLine();
                    System.out.println("Introduce la edad");
                    int edat = sc.nextInt();sc.nextLine();
                    System.out.println("Introduce el grado");
                    int grau = sc.nextInt();sc.nextLine();
                    System.out.println("Introduïu el id de visita");
                    int visitid = sc.nextInt();sc.nextLine();
                    System.out.println("Introduce la condena restante");
                    int condemnaRes = sc.nextInt();sc.nextLine();
                    System.out.println("Reclusa: " + reclusasDao.create(new Reclusa(nom, cognom, edat, grau, visitid, condemnaRes)) + "creada");
                    break;
                }
                case 4->{
                    System.out.println("Introduce el id de la reclusa a modificar");
                    int idReclusa = sc.nextInt();sc.nextLine();
                    System.out.println("Introduce el nombre nuevo");
                    String nom = sc.nextLine();
                    System.out.println("Introduce el apellido nuevo");
                    String cognom = sc.nextLine();
                    System.out.println("Introduce la edad");
                    int edat = sc.nextInt();sc.nextLine();
                    System.out.println("Introduce el grado");
                    int grau = sc.nextInt();sc.nextLine();
                    System.out.println("Introduïu el id de visita");
                    int visitid = sc.nextInt();sc.nextLine();
                    System.out.println("Introduce la condena restante");
                    int condemnaRes = sc.nextInt();sc.nextLine();
                    Reclusa rNueva =  new Reclusa(idReclusa, nom, cognom, edat, grau, visitid, condemnaRes);
                    reclusasDao.update(rNueva);
                }
                case 5->{
                    System.out.println("Introduce que reclusa quieres eliminar");
                    int idReclusa = sc.nextInt();sc.nextLine();
                    reclusasDao.delete(idReclusa);
                    break;
                }
                case 6->{
                    break;
                }
                default->{
                    System.out.println("Introduce un numero valido del 0 al 6");
                    break;
                }
            }
            menuReclusas.menu();
            opcio = sc.nextInt();sc.nextLine();
        }


    }
}