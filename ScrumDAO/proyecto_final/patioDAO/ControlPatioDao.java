package scrum.sprintdao.proyecto_final.patioDAO;

import scrum.sprintdao.proyecto_final.modelos.ControlPatio;

import java.sql.SQLException;
import java.util.List;

public interface ControlPatioDao {
    public int create(ControlPatio controlPatio) throws SQLException;
    public ControlPatio read(int idSalidaPatio) throws SQLException;
    public List<ControlPatio> getPatio() throws SQLException;
}
