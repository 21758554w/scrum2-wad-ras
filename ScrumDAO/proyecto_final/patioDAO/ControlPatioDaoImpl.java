package scrum.sprintdao.proyecto_final.patioDAO;

import scrum.sprintdao.Matias.Conexion;
import scrum.sprintdao.proyecto_final.modelos.ControlPatio;
import scrum.sprintdao.proyecto_final.modelos.Reclusa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ControlPatioDaoImpl implements ControlPatioDao {

    static Connection con = Conexion.getConnection();
    @Override
    public int create(ControlPatio controlPatio) throws SQLException {
        String query = "insert into ControlPatio(ReclusaId,HoraEntrada,HoraSalida)values (?,?,?)";
        PreparedStatement ps =con.prepareStatement(query);

        //ps.setInt(1,controlPatio.getIdReclusa());
        ps.setString(2,controlPatio.getHoraSalidaPatio());
        ps.setString(3,controlPatio.getHoraRegreso());
        ps.executeUpdate();
        ResultSet rs =ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public ControlPatio read(int reclusaId) throws SQLException {
        String query = " select cp.*,r.* from ControlPatio cp JOIN Reclusas r ON r.ReclusasId = cp.ReclusaId where ReclusaId=?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1,reclusaId);
        ControlPatio controlPatio =new ControlPatio();
        ResultSet rs = ps.executeQuery();
        boolean check = false;

        while (rs.next()){
            check=true;

            controlPatio.setIdPatio(rs.getInt("PatioId"));
            int reclusa = rs.getInt("ReclusasId");
            Reclusa r = new Reclusa();
            r.setReclusasId(reclusa);
            controlPatio.setIdReclusa(r);
            //controlPatio.setIdReclusa(rs.getInt("ReclusaId"));
            controlPatio.setHoraSalidaPatio(rs.getString("HoraEntrada"));
            controlPatio.setHoraSalidaPatio(rs.getString("HoraSalida"));
        }
        if (check) return controlPatio;
        else return null;
    }

    @Override
    public List<ControlPatio> getPatio() throws SQLException {
        String query ="select * from ControlPatio";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<ControlPatio> ls =new ArrayList<>();
        while (rs.next()){
            ControlPatio controlPatio = new ControlPatio();
            controlPatio.setIdPatio(rs.getInt("PatioId"));
            //controlPatio.setIdReclusa(rs.getInt("ReclusaId"));
            controlPatio.setHoraSalidaPatio(rs.getString("HoraEntrada"));
            controlPatio.setHoraRegreso(rs.getString("HoraSalida"));
            ls.add(controlPatio);
        }
        return ls;
    }
}
