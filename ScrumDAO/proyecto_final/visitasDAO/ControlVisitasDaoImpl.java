package scrum.sprintdao.proyecto_final.visitasDAO;

import scrum.sprintdao.proyecto_final.conexion.Conection;
import scrum.sprintdao.proyecto_final.modelos.ControlVisitas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ControlVisitasDaoImpl implements ControlVisitasDao {

    static Connection con = Conection.getConnection();

    @Override
    public int create(ControlVisitas controlVisitas) throws SQLException {

        int nuevoIdVisitas;
        String query = "insert into Visitas(Fecha,Hora,ReclusaId,VisitanteId) values (?,?,?,?)";
        try (PreparedStatement ps = con.prepareStatement(query)) {

            ps.setString(1, controlVisitas.getFecha());
            ps.setString(2, controlVisitas.getHora());
            ps.setInt(3, controlVisitas.getReclusaid());
            ps.setInt(4, controlVisitas.getVisitanteId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            nuevoIdVisitas = rs.getInt(1);
        }
        return nuevoIdVisitas;

    }

    @Override
    public ControlVisitas read(int idVisita) throws SQLException {
        String query = "select * from Visitas where VisitaId =?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, idVisita);
        ControlVisitas controlVisitas = new ControlVisitas();
        ResultSet rs = ps.executeQuery();
        boolean check = false;
        while (rs.next()) {
            check = true;
            controlVisitas.setIdVisita(rs.getInt("VisitaId"));
            controlVisitas.setFecha(rs.getString("Fecha"));
            controlVisitas.setHora(rs.getString("Hora"));
            controlVisitas.setReclusaid(rs.getInt("ReclusaId"));
            controlVisitas.setVisitanteId(rs.getInt("VisitanteId"));
        }
        if (check) {
            return controlVisitas;
        } else {
            return null;
        }
    }

    @Override
    public List<ControlVisitas> getRegister() throws SQLException {
        String query = "select * from Visitas";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<ControlVisitas> ls = new ArrayList<>();
        while (rs.next()) {
            ControlVisitas controlVisitas = new ControlVisitas();
            controlVisitas.setIdVisita(rs.getInt("VisitaId"));
            controlVisitas.setFecha(rs.getString("Fecha"));
            controlVisitas.setHora(rs.getString("Hora"));
            controlVisitas.setReclusaid(rs.getInt("ReclusaId"));
            controlVisitas.setVisitanteId(rs.getInt("VisitanteId"));
            ls.add(controlVisitas);
        }
        return ls;
    }
}
