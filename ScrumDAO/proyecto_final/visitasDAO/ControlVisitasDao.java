package scrum.sprintdao.proyecto_final.visitasDAO;

import scrum.sprintdao.proyecto_final.modelos.ControlVisitas;

import java.sql.SQLException;
import java.util.List;
public interface ControlVisitasDao {
    public int create(ControlVisitas controlVisitas) throws SQLException;
    public ControlVisitas read(int idVisita) throws SQLException;
    public List<ControlVisitas> getRegister() throws SQLException;


}
