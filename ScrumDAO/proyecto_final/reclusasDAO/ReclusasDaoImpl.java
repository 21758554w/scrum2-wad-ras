package scrum.sprintdao.proyecto_final.reclusasDAO;

import scrum.sprintdao.proyecto_final.conexion.Conection;
import scrum.sprintdao.proyecto_final.modelos.Reclusa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReclusasDaoImpl implements ReclusasDao {

    private static Connection con = Conection.getConnection();

    @Override
    public int create(Reclusa reclusa) throws SQLException {

        String query = "insert into Reclusas(Nombre, Apellido, Edad, Grado, CondenaRestante, VisitaId) VALUES (?, ?, ?, ?, ?, ?)";

        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, reclusa.getNombre());
        ps.setString(2, reclusa.getApellido());
        ps.setInt(3, reclusa.getEdad());
        ps.setInt(4, reclusa.getGrado());
        ps.setInt(5, reclusa.getCondenaRestante());
        ps.setInt(6, reclusa.getIdVisita());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        rs.next();

        return rs.getInt(1);
    }

    @Override
    public void delete(int reclusasId) throws SQLException {

        String query = "delete from Reclusas where ReclusasId = ?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, reclusasId);
        ps.executeUpdate();
    }

    @Override
    public Reclusa read(int reclusasId) throws SQLException {

        String query = "select * from Reclusas where ReclusasId = ?";
        PreparedStatement ps = con.prepareStatement(query);

        ps.setInt(1, reclusasId);
        Reclusa reclusa = new Reclusa();
        ResultSet rs = ps.executeQuery();
        boolean newReclusa = false;

        while (rs.next()) {
            reclusa.setReclusasId(rs.getInt(1));
            reclusa.setNombre(rs.getString(2));
            reclusa.setApellido(rs.getString(3));
            reclusa.setEdad(rs.getInt(4));
            reclusa.setGrado(rs.getInt(5));
            reclusa.setCondenaRestante(rs.getInt(6));
            reclusa.setIdVisita(rs.getInt(7));
            newReclusa = true;
        }
        if (newReclusa) return reclusa;
        else return null;
    }

    @Override
    public void update(Reclusa reclusa) throws SQLException {
        try {

            String query = "UPDATE Reclusas set Nombre =?, Apellido = ?, Edad = ?, Grado = ?, CondenaRestante = ?, VisitaId = ? where ReclusasId = ?";

            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, reclusa.getNombre());
            ps.setString(2, reclusa.getApellido());
            ps.setInt(3, reclusa.getEdad());
            ps.setInt(4, reclusa.getGrado());
            ps.setInt(5, reclusa.getCondenaRestante());
            ps.setInt(6, reclusa.getIdVisita());
            ps.setInt(7, reclusa.getReclusasId());
            ps.executeUpdate();
            System.out.println("Operation done successfully");
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public List<Reclusa> getReclusas() throws SQLException {
        String query = "select * from Reclusas";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Reclusa> reclusas = new ArrayList<>();

        while (rs.next()) {
            Reclusa reclusa = new Reclusa();
            reclusa.setReclusasId(rs.getInt(1));
            reclusa.setNombre(rs.getString(2));
            reclusa.setApellido(rs.getString(3));
            reclusa.setEdad(rs.getInt(4));
            reclusa.setGrado(rs.getInt(5));
            reclusa.setCondenaRestante(rs.getInt(6));
            reclusa.setIdVisita(rs.getInt(7));
            reclusas.add(reclusa);
        }
        return reclusas;
    }
}
