package scrum.sprintdao.proyecto_final.reclusasDAO;


import scrum.sprintdao.proyecto_final.modelos.Reclusa;

import java.sql.SQLException;
import java.util.List;

public interface ReclusasDao {

    public int create(Reclusa reclusa) throws SQLException;
    public void delete(int reclusasId) throws SQLException;
    public Reclusa read(int reclusasId) throws SQLException;
    public void update(Reclusa reclusa) throws SQLException;
    public List<Reclusa> getReclusas() throws SQLException;
}
