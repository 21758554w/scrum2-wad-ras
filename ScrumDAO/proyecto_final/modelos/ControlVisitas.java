package scrum.sprintdao.proyecto_final.modelos;

import java.util.ArrayList;
import java.util.Scanner;

public class ControlVisitas  {
    private int idVisita;
    private String fecha;
    private String hora;
    private int reclusaid;
    private int visitanteId;

    public ControlVisitas(int idVisita, String fecha, String hora, int reclusaid, int visitanteId) {
        this.idVisita = idVisita;
        this.fecha = fecha;
        this.hora = hora;
        this.reclusaid = reclusaid;
        this.visitanteId = visitanteId;
    }
    public ControlVisitas(){
    }

    public int getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getReclusaid() {
        return reclusaid;
    }

    public void setReclusaid(int reclusaid) {
        this.reclusaid = reclusaid;
    }

    public int getVisitanteId() {
        return visitanteId;
    }

    public void setVisitanteId(int visitanteId) {
        this.visitanteId = visitanteId;
    }

    @Override
    public String toString() {
        return "\nscrum.sprintdao.proyecto_final.modelos.ControlVisitas{" +
                "idVisita=" + idVisita +
                ", fecha='" + fecha + '\'' +
                ", hora='" + hora + '\'' +
                ", reclusaid=" + reclusaid +
                ", visitanteId=" + visitanteId +
                '}';
    }
}
