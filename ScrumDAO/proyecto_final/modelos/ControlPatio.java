package scrum.sprintdao.proyecto_final.modelos;

import scrum.sprintdao.proyecto_final.modelos.Reclusa;


public class ControlPatio {
    private int idPatio;
    private Reclusa idReclusa;
    private String horaSalidaPatio;
    private String horaRegreso = "12:00";

    public ControlPatio(int idPatio,Reclusa idReclusa, String horaSalidaPatio, String horaRegreso) {
        this.idPatio = idPatio;
        this.idReclusa = idReclusa;
        this.horaSalidaPatio = horaSalidaPatio;
        this.horaRegreso = horaRegreso;
    }

    public ControlPatio() {
    }

    public int getIdPatio() {
        return idPatio;
    }

    public void setIdPatio(int idPatio) {
        this.idPatio = idPatio;
    }

    public Reclusa getIdReclusa() {
        return idReclusa;
    }

    public void setIdReclusa(Reclusa idReclusa) {
        this.idReclusa = idReclusa;
    }

    public String getHoraSalidaPatio() {
        return horaSalidaPatio;
    }

    public void setHoraSalidaPatio(String horaSalidaPatio) {
        this.horaSalidaPatio = horaSalidaPatio;
    }

    public String getHoraRegreso() {
        return horaRegreso;
    }

    public void setHoraRegreso(String horaRegreso) {
        this.horaRegreso = horaRegreso;
    }

    @Override
    public String toString() {
        return "ControlPatio{" +
                "idPatio=" + idPatio +
                ", idReclusa=" + idReclusa +
                ", horaSalidaPatio='" + horaSalidaPatio + '\'' +
                ", horaRegreso='" + horaRegreso + '\'' +
                '}';
    }
}