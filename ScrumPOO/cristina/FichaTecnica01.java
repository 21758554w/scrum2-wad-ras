import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class FichaTecnica01 {

    public static void main(String[] args) throws IOException {
        FichaTecnica01 ft = new FichaTecnica01();
        ft.importarCSV();
    }

    public void importarCSV() throws IOException {
        HashMap<String, Reclusa> resultadoArchivo = new HashMap<>();

        FileReader fr = new FileReader("reclusas.csv");
        BufferedReader br = new BufferedReader(fr);
        String line;
        int i = 0;
        //id,dni,nombre,apellido,edad,grado,diaVisit
        try {
            while ((line = br.readLine()) != null && i < 5) {
                System.out.println(line);
                String[] strings = line.split(",");
                String dni = strings[0];
                String nombre = strings[1];
                String apellido = strings[2];
                int edad = Integer.parseInt(strings[3]);
                int grado = Integer.parseInt(strings[4]);
                int diaVisita = Integer.parseInt(strings[5]);
                Reclusa reclusa = new Reclusa(nombre, apellido, edad, grado, diaVisita);
                resultadoArchivo.put(dni, reclusa);
                i++;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.println(resultadoArchivo);
    }
}

    /*public void leerDatosCSV(String data) {
        HashMap<String, Reclusa> reclusaList = new HashMap<>();

        try {
            FileReader fr = new FileReader("Reclusasasdf.csv");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            String[] strings = line.split(",");
            String dni = strings[0];

            String id = strings[1];
            String nombre = strings[2];
            String apellido = strings[3];
            int edad = Integer.parseInt(strings[4]);
            Persona persona = new Persona(id, nombre, apellido, edad);

            int grado = Integer.parseInt(strings[5]);
            int diaVisita = Integer.parseInt(strings[6]);
            Reclusa reclusa = new Reclusa(persona, grado, diaVisita);


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
*/