package uf2;

public class Reclusas {

    public static final String tipoGradoCondena = "3";
    public static int presasSalirPatio;
    public static boolean permiso;

    public static String[][] fichasTecnicasReclusas = {
            //id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio
            {"0001", "Juana Nascimento", "29", "20", "11", "3", ""},
            {"0002", "Giovana Libert", "42", "10", "5", "1", ""},
            {"0003", "Maria Lisa", "33", "8", "5", "3", ""},
            {"0004", "Marta Eulalia", "39", "4", "2", "2", ""},
            {"0005", "Carmen ", "65", "5", "1", "2", ""},
            {"0006", "Ana Beltran", "32", "10", "7", "3", ""},
            {"0007", "Andrew Gil", "40", "10", "10", "2", ""},
            {"0008", "Camila Jimenez", "37", "20", "17", "2", ""},
            {"0009", "Andrea Linares", "42", "5", "9", "3", ""},
            {"0010", "Laura Ochoa", "25", "15", "7", "3", ""},
            {"0011", "Juan Peres", "22", "6", "3", "2", ""},
            {"0012", "Pedro Linares", "20", "11", "5", "4", ""},
            {"0013", "Carla Sespedes", "35", "10", "8", "4", ""},
            {"0014", "Karol Escovar", "60", "19", "10", "3", ""},
            {"0015", "Daru Garviria", "25", "14", "9", "3", ""},
    };

    public static void mostrarFichaTecnica() {
        System.out.println("id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio");
        for (String[] filas : fichasTecnicasReclusas) {
            for (String colum : filas) {
                System.out.print(colum + " ");
            }
            System.out.println();
        }
    }

    public static void establecerEstadoSalidaPatioReclusas() {
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) < 9) {
                fichasTecnicasReclusas[i][6] = "1";
            }else {
                fichasTecnicasReclusas[i][6] = "0";
            }
        }
    }

    public static int mostrarReclusasPatioDisponible() {
        System.out.println("\033[34mRECLUSAS DE TERCER GRADO QUE PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("1")) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
                presasSalirPatio=i;
            }
        }
        return presasSalirPatio;
    }

    public static void mostrarReclusasPatioProhibido() {
        System.out.println("\033[31mRECLUSAS DE TERCER GRADO QUE NO PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("0") && fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) >= 9) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
            }
        }
    }
    public static boolean presasConPermiso(){
        permiso=false;
        for (int i = 0; i < ControlReclusas.controlReclusas.length; i++) {

            int indiceFichaPresas=mostrarReclusasPatioDisponible();
            if (ControlReclusas.controlReclusas[i].equals(indiceFichaPresas)){
                permiso =true;
            }
            else
               permiso= false;
        }
        return permiso;
    }

    public static void agregarReclusas(int numero) {
        String[][] nuevasReclusas = new String[numero][5];
    }
}
public class Reclusas {


    public static final String tipoGradoCondena = "3";
    public static int presasSalirPatio;
    public static boolean permiso;

    public static String[][] fichasTecnicasReclusas = {
            //id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio
            {"0001", "Juana Nascimento", "29", "20", "11", "3", ""},
            {"0002", "Giovana Libert", "42", "10", "5", "1", ""},
            {"0003", "Maria Lisa", "33", "8", "5", "3", ""},
            {"0004", "Marta Eulalia", "39", "4", "2", "2", ""},
            {"0005", "Carmen ", "65", "5", "1", "2", ""},
            {"0006", "Ana Beltran", "32", "10", "7", "3", ""},
            {"0007", "Andrew Gil", "40", "10", "10", "2", ""},
            {"0008", "Camila Jimenez", "37", "20", "17", "2", ""},
            {"0009", "Andrea Linares", "42", "5", "9", "3", ""},
            {"0010", "Laura Ochoa", "25", "15", "7", "3", ""},
    };

    public static void mostrarFichaTecnica() {
        System.out.println("id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio");
        for (String[] filas : fichasTecnicasReclusas) {
            for (String colum : filas) {
                System.out.print(colum + " ");
            }
            System.out.println();
        }
    }

    public static void establecerEstadoSalidaPatioReclusas() {
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) < 9) {
                fichasTecnicasReclusas[i][6] = "1";
            } else {
                fichasTecnicasReclusas[i][6] = "0";
            }
        }
    }

    public static int reclusasPatioDisponible() {//cambiadaaaaaaaaaaaaaaaaaaa
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("1")) {
                presasSalirPatio = i;
            }
        }
        return presasSalirPatio;
    }
    public static void mostrarReclusasPatio() {
        System.out.println("\033[34mRECLUSAS DE TERCER GRADO QUE PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("1")) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
            }
        }
    }

    public static void mostrarReclusasPatioProhibido() {
        System.out.println("\033[31mRECLUSAS DE TERCER GRADO QUE NO PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("0") && fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) >= 9) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
            }
        }
    }

    public static void presasConPermiso() {
        for (int i = 0; i < reclusasPatioDisponible(); i++) {
            if (Integer.parseInt(ControlReclusas.controlReclusas[i][0]) == reclusasPatioDisponible()) {
//                System.out.println(mostrarReclusasPatioDisponible());
                System.out.println(ControlReclusas.controlReclusas[i][0]);
            }
        }
    }

    public static void agregarReclusas(int numero) {
        String[][] nuevasReclusas = new String[numero][5];
    }
}
//*************************************************************************************************************** */
public class Reclusas {


    public static final String tipoGradoCondena = "3";
    public static int presasSalirPatio;
    public static boolean permiso;

    public static String[][] fichasTecnicasReclusas = {
            //id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio
            {"0001", "Juana Nascimento", "29", "20", "11", "3", ""},
            {"0002", "Giovana Libert", "42", "10", "5", "1", ""},
            {"0003", "Maria Lisa", "33", "8", "5", "3", ""},
            {"0004", "Marta Eulalia", "39", "4", "2", "2", ""},
            {"0005", "Carmen ", "65", "5", "1", "2", ""},
            {"0006", "Ana Beltran", "32", "10", "7", "3", ""},
            {"0007", "Andrew Gil", "40", "10", "10", "2", ""},
            {"0008", "Camila Jimenez", "37", "20", "17", "2", ""},
            {"0009", "Andrea Linares", "42", "5", "9", "3", ""},
            {"0010", "Laura Ochoa", "25", "15", "7", "3", ""},
    };

    public static void mostrarFichaTecnica() {
        System.out.println("id, nombre, dni, edad, sentencia, condena restante, tipo grado, salida patio");
        for (String[] filas : fichasTecnicasReclusas) {
            for (String colum : filas) {
                System.out.print(colum + " ");
            }
            System.out.println();
        }
    }

    public static void establecerEstadoSalidaPatioReclusas() {
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) < 9) {
                fichasTecnicasReclusas[i][6] = "1";
            } else {
                fichasTecnicasReclusas[i][6] = "0";
            }
        }
    }

    public static boolean estaDisponibleRecusaPatio(int index) {

            return fichasTecnicasReclusas[index][6].equals("1");


    }
    public static void mostrarReclusasPatio() {
        System.out.println("\033[34mRECLUSAS DE TERCER GRADO QUE PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("1")) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
            }
        }
    }

    public static void mostrarReclusasPatioProhibido() {
        System.out.println("\033[31mRECLUSAS DE TERCER GRADO QUE NO PUEDEN SALIR AL PATIO:\033[0m");
        for (int i = 0; i < fichasTecnicasReclusas.length; i++) {
            if (fichasTecnicasReclusas[i][6].equals("0") && fichasTecnicasReclusas[i][5].equals(tipoGradoCondena) && Integer.parseInt(fichasTecnicasReclusas[i][4]) >= 9) {
                System.out.println(fichasTecnicasReclusas[i][0] + " " + fichasTecnicasReclusas[i][1]);
            }
        }
    }
//necesito utilizar el indice para compararlo con la otra matriz y de la otra matriz sacar el horario y eso
    /*public static void presasConPermiso() {
        for (int i = 0; i < reclusasPatioDisponible(); i++) {
            if (Integer.parseInt(ControlReclusas.controlReclusas[i][0]) == reclusasPatioDisponible()) {
//                System.out.println(mostrarReclusasPatioDisponible());
                System.out.println(ControlReclusas.controlReclusas[i][0]);
            }
        }
    }
*/
    public static void agregarReclusas(int numero) {
        String[][] nuevasReclusas = new String[numero][5];
    }
}
