## Mejoras a realizar en el equipo:
Listar las cosas que creen que habria que mejorar en el equipo al momento distribuir todo:


### Daruny :
- 1 al momento de planificar podriamos hacerlo todos, dar ideas cada uno y despues elegir cuales si y cuales no.
- 2 trabajar indivualmente al momento de crear funcionalidades para poder ser mas productivos
- 3 intentar hacer codigo limpio
- 4 mejorar la comunicacion entre todos

### Cristina:

- 1 mejorar la planificacion del poryecto de manera que cada miembro del grupo aporte ideas y soluciones, para luego implementarlas.

- 2 hacer el codigo mas limpio y menos repetitivo.

- 3  mejorar la comuniaciion entre nosotros.


- 4


### Kevin:

- 1

- 2

- 3

- 4


### Matias:

- 1

- 2

- 3

- 4

### Karol:

- 1. Mejorar la planificacion y estructuracion antes de empezar a trabajar para optimizar labores.

## Mejoras a realizar para el siguiente Sprint:
Las mejoras que podemos hacer en proyecto , ideas para agregar y cuando llegue el dia debatir lo que si y lo que no.

### Daruny:
- 1 cambiar la estructura de datos del projecto.
- 2 reorganizar el UML.

### Cristina:

- 1 implementar lo aprendido en los scrums anteriores y lo aprendido en clase.

- 2

- 3

- 4


### Kevin:

- 1

- 2

- 3

- 4


### Matias:

- 1

- 2

- 3

- 4

### Karol:

- 1. Implementar lo aprendido en este sprint en cuanto a la estructuracion y planificacion del codigo, antes de la creacion.

- 2. Mejorar la comunicación en cuanto a informar cambios de perspectivas o de ideas sobre el desarrollo del codigo para llevar las actualizaciónes al dia y analizar los efectos sobre las ideas ya planteadas en cuanto a codigo.

- 3. Unificar el  codigo creado por los integrantes del grupo para que nadie se sienta sobrecargado.  